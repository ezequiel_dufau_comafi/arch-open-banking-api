const axios = require('axios');
const logger = require('logger')(module);
const url = 'http://checkip.amazonaws.com/';
let response;

/**
 * Aqui debe tener una descripcion de la ejecucíon de la lambda, esto será parte de la mediciones de
 * SonarQube en base al standar : https://docs.sonarqube.org/latest/user-guide/metric-definitions/
 * y del equipo de devops: http://openproject.developmentcomafi.com:8080/projects/portal-rm-devops/wiki/agrupaciones-e-indicadores
 *
 * @param event
 * @param context
 * @returns {Promise<*>}
 *
 * @author <<nombre y apellido>> - <<email>>
 */

exports.handler = async (event, context) => {
    logger.log({ info: "ESTO ES UN MENSAJE DESDE LA LAYER CUSTOM logger" });
    try {
        const ret = await axios(url);
        response = {
            'statusCode': 200,
            'body': JSON.stringify({
                message: 'hello world',
                location: ret.data.trim()
            })
        };
    } catch (err) {
        logger.log({ error: err, info: "ESTO ES UN MENSAJE DESDE LA LAYER CUSTOM logger" });
        response = {
          statusCode: 500,
          body: JSON.stringify({
              message: "Error"
          })
        };
    }

    return response;
};
