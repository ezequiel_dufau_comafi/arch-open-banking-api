""" Importación de layers hechas en el proyecto actual:

from sys import path
path.insert(0, '/opt')
import Lib_layer_pyX.layer as layer_pyX
"""

"""
Para importar librerías de la baselayer:
Ejemplo teniendo requests en la baselayer

import requests
"""
import requests

import json


def layer1_f1(name):

    try:

        ## Insert here your function's code
        ##########################
        ip = requests.get("http://checkip.amazonaws.com/")
        print("Esta es la ip: " + ip.text)
        return  {
                    "message": "Hola desde lambda PY " + name,
                    "location": ip.text.replace("\n", ""),
                    "layer_py": 1
                }
        ##########################

    except requests.RequestException as e:
        # Send some context about this error to Lambda Logs
        print(e)

        raise e

