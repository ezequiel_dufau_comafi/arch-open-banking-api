![image alt ><](https://d2908q01vomqb2.cloudfront.net/77de68daecd823babbb58edb1c8e14d7106e83bb/2018/12/21/Lambda-Layers-1.jpg)

Documentacion oficial: [aquí](https://docs.aws.amazon.com/lambda/latest/dg/configuration-layers.html)

# Descripción

Estructura para la creacion y despliegue de layer's en diferentes plataformas:

* Node JS [ver instructivo](node/README.md)
* Python [ver instructivo](python/README.md)
* Java [ver instructivo](java/README.md)
